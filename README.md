Toward's Data Science - NLP Practitioner's Guide
====================

I've been working through [Toward's Data Science]'s [A Practitioner's Guide to NLP].

It's a great guide and I'm enjoying working through with it. This repo has the notebooks and python files. 

[Toward's Data Science]: https://towardsdatascience.com/
[A Practitioner's Guide to NLP]: https://towardsdatascience.com/a-practitioners-guide-to-natural-language-processing-part-i-processing-understanding-text-9f4abfd13e72